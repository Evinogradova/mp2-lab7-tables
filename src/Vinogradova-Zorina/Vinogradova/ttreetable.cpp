#include "ttreetable.h"

void TTreeTable::DeleteTreeTab(PTTreeNode pNode)
{
    if (pNode != nullptr)
    {
        DeleteTreeTab(pNode->pLeft);
        DeleteTreeTab(pNode->pRight);
        delete pNode;
    }
}

PTDatValue TTreeTable::FindRecord(TKey k)
{
    PTTreeNode pNode = pRoot;
    ppRef = &pRoot;
    while (pNode != nullptr)
    {
        if (pNode->Key == k)
            break;
        else if (pNode->Key > k)
            ppRef = &pNode->pLeft;
        else
            ppRef = &pNode->pRight;
        pNode = *ppRef;
    }
    return (pNode == nullptr) ? nullptr : pNode->pValue;
}

void TTreeTable::InsRecord(TKey k, PTDatValue pVal)
{
    if (FindRecord(k) == nullptr)
    {
        *ppRef = new TTreeNode(k, pVal);
        DataCount++;
    }
}

void TTreeTable::DelRecord(TKey k)
{
    if (FindRecord(k) != nullptr)
    {
        PTTreeNode tmp = pRoot, up;

        while (!St.empty())
            St.pop();
        while (tmp->Key != k)
        {
            St.push(tmp);
            if (tmp->Key > k)
                tmp = tmp->pLeft;
            else
                tmp = tmp->pRight;
        }

        if (tmp->pLeft != nullptr && tmp->pRight != nullptr)
        {
            PTTreeNode down_left = tmp->pRight;
            while (down_left->pLeft != nullptr)
                down_left = down_left->pLeft;
            down_left->pLeft = tmp->pLeft;

            if (!St.empty())
            {
                up = St.top();
                if (up != nullptr)
                {
                    if (up->pLeft == tmp)
                        up->pLeft = nullptr;
                    else if (up->pRight == tmp)
                        up->pRight = nullptr;
                }
            }
            else
                pRoot = tmp->pRight;
        }
        else
        {
            if (!St.empty())
            {
                up = St.top();
                if (up != nullptr)
                {
                    if (up->pLeft == tmp)
                        up->pLeft = nullptr;
                    else if (up->pRight == tmp)
                        up->pRight = nullptr;
                }
            }
            else
            {
                if (tmp->pLeft == nullptr && tmp->pRight == nullptr)
                    pRoot = nullptr;
                else if (tmp->pLeft != nullptr && tmp->pRight == nullptr)
                    pRoot = tmp->pLeft;
                else if (tmp->pLeft == nullptr && tmp->pRight != nullptr)
                    pRoot = tmp->pRight;
            }
        }
        delete tmp;
        DataCount--;
    }
}

TKey TTreeTable::GetKey() const
{
    return (pCurrent == nullptr) ? "" : pCurrent->Key;
}

PTDatValue TTreeTable::GetValuePTR() const
{
    return (pCurrent == nullptr) ? nullptr : pCurrent->pValue;
}

void TTreeTable::Reset()
{
    PTTreeNode pNode = pCurrent = pRoot;
    while (!St.empty())
        St.pop();
    CurrPos = 0;
    while (pNode != nullptr)
    {
        St.push(pNode);
        pCurrent = pNode;
        pNode = pNode->pLeft;
    }
}

bool TTreeTable::IsTabEnded() const
{
    return CurrPos >= DataCount;
}

void TTreeTable::GoNext()
{
    CurrPos++;
    if (!IsTabEnded() && (pCurrent != nullptr))
    {
        PTTreeNode pNode = pCurrent = pCurrent->GetRight();
        St.pop();
        while (pNode != nullptr)
        {
            St.push(pNode);
            pCurrent = pNode;
            pNode = pNode->GetLeft();
        }
        if ((pCurrent == nullptr) && !St.empty())
            pCurrent = St.top();
    }
}