#include "tscantable.h"

PTDatValue TScanTable::FindRecord(TKey k)
{
    PTDatValue res = nullptr;
    for (int i = 0; i < DataCount; i++)
    {
        if (pRecs[i]->Key == k)
        {
            CurrPos = i;
            res = pRecs[i]->pValue;
            break;
        }
    }
    return res;
}

void TScanTable::InsRecord(TKey k, PTDatValue pVal)
{
    for (int i = 0; i < TabSize; i++)
    {
        if (pRecs[i] == nullptr)
        {
            pRecs[i] = new TTabRecord(k, pVal);
            DataCount++;
            break;
        }
    }
}

void TScanTable::DelRecord(TKey k)
{
    for (int i = 0; i < TabSize; i++)
    {
        if (pRecs[i] != nullptr && pRecs[i]->Key == k)
        {
            delete pRecs[i];
            pRecs[i] = pRecs[DataCount - 1];
            pRecs[DataCount - 1] = nullptr;
            DataCount--;
        }
    }
}