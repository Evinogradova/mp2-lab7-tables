#ifndef __TREETABLE_H__
#define __TREETABLE_H__

#include <stack>
#include "ttable.h"
#include "ttreenode.h"

class  TTreeTable : public TTable
{
protected:
    PTTreeNode pRoot;       // ��������� �� ������ ������
    PTTreeNode *ppRef;      // ����� ��������� �� �������-���������� � FindRecord
    PTTreeNode pCurrent;    // ��������� �� ������� �������
    int CurrPos;            // ����� ������� �������
    stack < PTTreeNode> St; // ���� ��� ���������
    void DeleteTreeTab(PTTreeNode pNode); // ��������
public:
    TTreeTable() : TTable(), CurrPos(0), ppRef(nullptr) { pRoot = pCurrent = nullptr; }
    ~TTreeTable() { DeleteTreeTab(pRoot); } // ����������
    // �������������� ������
    virtual bool IsFull() const { return false; } //���������?
    //�������� ������
    virtual PTDatValue FindRecord(TKey k);           // ����� ������
    virtual void InsRecord(TKey k, PTDatValue pVal); // ��������
    virtual void DelRecord(TKey k);                  // ������� ������
    // ���������
    virtual TKey GetKey() const;
    virtual PTDatValue GetValuePTR() const;
    virtual void Reset();            // ���������� �� ������ ������
    virtual bool IsTabEnded() const; // ������� ���������?
    virtual void GoNext();           // ������� � ��������� ������
    //(=1 ����� ���������� ��� ��������� ������ �������)
};

#endif