#ifndef __ARRAYTABLE_H__
#define __ARRAYTABLE_H__

#include "ttable.h"

#define TabMaxSize 25
enum TDataPos { FIRST_POS, CURRENT_POS, LAST_POS };

class  TArrayTable : public TTable
{
protected:
    PTTabRecord *pRecs; // ������ ��� ������� �������
    int TabSize;        // ����. ����.���������� ������� � �������
    int CurrPos;        // ����� ������� ������ (��������� � 0)
public:
    TArrayTable(int Size = TabMaxSize);
    ~TArrayTable();
    // �������������� ������
    virtual bool IsFull() const { return DataCount >= TabSize; }
    int GetTabSize() const { return TabSize; }
    // ������
    virtual TKey GetKey() const { return GetKey(CURRENT_POS); }
    virtual PTDatValue GetValuePTR() const { return GetValuePTR(CURRENT_POS); }
    virtual TKey GetKey(TDataPos mode) const;
    virtual PTDatValue GetValuePTR(TDataPos mode) const;
    // �������� ������
    virtual PTDatValue FindRecord(TKey k) = 0;           // ����� ������
    virtual void InsRecord(TKey k, PTDatValue pVal) = 0; // ��������
    virtual void DelRecord(TKey k) = 0;                  // ������� ������
    //���������
    virtual void Reset() { CurrPos = 0; }                            // ���������� �� ������ ������
    virtual bool IsTabEnded() const { return CurrPos >= DataCount; } // ������� ���������?
    virtual void GoNext();                                           // ������� � ��������� ������
    //(=1 ����� ���������� ��� ��������� ������ �������)
    virtual void SetCurrentPos(int pos);                             // ���������� ������� ������
    int GetCurrentPos() const { return CurrPos; }                    //�������� ����� ������� ������

    friend TSortTable;
};

#endif