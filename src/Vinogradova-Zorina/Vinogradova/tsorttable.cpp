#include "tsorttable.h"

void TSortTable::SortData()
{
    switch (SortMethod)
    {
    case INSERT_SORT:
        InsertSort(pRecs, DataCount);
        break;
    case MERGE_SORT:
        MergeSort(pRecs, DataCount);
        break;
    case QUIQ_SORT:
        QuiqSort(pRecs, DataCount);
        break;
    }
}

void TSortTable::InsertSort(PTTabRecord *pMem, int DataCount) //????????????????????????
{
    PTTabRecord pR;
    for (int i = 1; i < DataCount; i++)
    {
        pR = pRecs[i];
        int j = i - 1;
        while (j >= 0 && pRecs[j]->Key > pR->Key)
        {
            pRecs[j + 1] = pRecs[j];
            j--;
        }
        pRecs[j + 1] = pR;
    }
}

void TSortTable::MergeSort(PTTabRecord *pMem, int DataCount)
{
    PTTabRecord *pData = pRecs;
    PTTabRecord *pBuff = new PTTabRecord[DataCount];
    PTTabRecord *pTemp = pBuff;
    MergeSorter(pData, pBuff, DataCount);
    if (pData == pTemp)
    {
        for (int i = 0; i < DataCount; i++)
            pBuff[i] = pData[i];
    }
    delete pTemp;
}

void TSortTable::MergeSorter(PTTabRecord * &pData, PTTabRecord * &pBuff, int Size)
{
    int n1 = (Size + 1) / 2;
    int n2 = Size - n1;
    if (Size > 2)
    {
        PTTabRecord *pDat2 = pData + n1, *pBuff2 = pBuff + n1;
        MergeSorter(pData, pBuff, n1);
        MergeSorter(pDat2, pBuff2, n2);
    }
    MergeData(pData, pBuff, n1, n2);
}

void TSortTable::MergeData(PTTabRecord *&pData, PTTabRecord *&pBuff, int n1, int n2)
{
    for (int i = 0; i < (n1 + n2); i++)
        pBuff[i] = pData[i];
    PTTabRecord *&tmp = pData;
    pData = pBuff;
    pBuff = tmp;
}

void TSortTable::QuiqSort(PTTabRecord *pMem, int DataCount)
{
    int pivot = 0;
    int n1, n2;
    if (DataCount > 1) {
        QuiqSplit(pRecs, DataCount, pivot);
        n1 = pivot + 1;
        n2 = DataCount - n1;
        QuiqSort(pRecs, n1 - 1);
        QuiqSort(pRecs + n1, n2);
    }
}

void TSortTable::QuiqSplit(PTTabRecord *pData, int Size, int &Pivot)
{
    PTTabRecord pPivot = pData[0], pTemp;
    int i1 = 1, i2 = Size - 1;
    while (i1 <= i2) {
        while ((i1 < Size) && !(pData[i1]->GetKey() > pPivot->GetKey())) i1++;
        while (pData[i2]->GetKey() > pPivot->GetKey()) i2--;
        if (i1 < i2) {
            pTemp = pData[i1];
            pData[i1] = pData[i2];
            pData[i2] = pTemp;
        }
    }
    pData[0] = pData[i2];
    pData[i2] = pPivot;
    Pivot = i2;
}

TSortTable::TSortTable(const TScanTable &tab)
{
    *this = tab;
}

TSortTable & TSortTable::operator=(const TScanTable &tab)
{
    if (pRecs != nullptr)
    {
        for (int i = 0; i < DataCount; i++)
            delete pRecs[i];
        delete[] pRecs;
        pRecs = nullptr;
    }
    TabSize = tab.TabSize;
    DataCount = tab.DataCount;
    pRecs = new PTTabRecord[TabSize];
    for (int i = 0; i < DataCount; i++)
        pRecs[i] = (PTTabRecord)tab.pRecs[i]->GetCopy();
    SortData();
    CurrPos = 0;
    return *this;
}

// �������� ������

PTDatValue TSortTable::FindRecord(TKey k)
{
    PTDatValue res = nullptr;
    if (DataCount != 0)
    {
        int i, iup = 0, idown = DataCount - 1;
        while (iup <= idown)
        {
            i = (iup + idown) / 2;
            if (pRecs[i]->Key == k)
            {
                res = pRecs[i]->pValue;
                break;
            }
            else if (pRecs[i]->Key > k)
                idown = i - 1;
            else
                iup = i + 1;
        }
    }
    return res;
}

void TSortTable::InsRecord(TKey k, PTDatValue pVal)  //????????????????
{
    if (!IsEmpty())
    {
        if (FindRecord(k) == nullptr)
        {
            pRecs[DataCount + 1] = new TTabRecord(k, pVal);
            DataCount++;
            SortData();
        }
    }
}

void TSortTable::DelRecord(TKey k)
{
    if (!IsEmpty())
    {
        for (int i = 0; i < DataCount; i++)
        {
            if (pRecs[i]->Key == k)
                delete pRecs[i];
            for (int j = i; j < DataCount; j++)
                pRecs[j] = pRecs[j + 1];
            DataCount--;
        }
    }
}