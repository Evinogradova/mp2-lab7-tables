#include "tarraytable.h"

TArrayTable::TArrayTable(int Size)
{
    TabSize = Size;
    CurrPos = 0;
    pRecs = new PTTabRecord[Size];
    for (int i = 0; i < Size; ++i)
        pRecs[i] = nullptr;
}

TArrayTable::~TArrayTable()
{
    for (int i = 0; i < DataCount; ++i)
        delete pRecs[i];
    delete[] pRecs;
}

TKey TArrayTable::GetKey(TDataPos mode) const
{
    int pos = -1;
    if (!IsEmpty())
    {
        switch (mode)
        {
        case FIRST_POS:
            pos = 0;
            break;
        case CURRENT_POS:
            pos = CurrPos;
            break;
        case LAST_POS:
            pos = DataCount - 1;
            break;
        }
    }
    return (pos == -1) ? "" : pRecs[pos]->Key;
}

PTDatValue TArrayTable::GetValuePTR(TDataPos mode) const
{
    int pos = -1;
    if (!IsEmpty())
    {
        switch (mode)
        {
        case FIRST_POS:
            pos = 0;
            break;
        case CURRENT_POS:
            pos = CurrPos;
            break;
        case LAST_POS:
            pos = DataCount - 1;
            break;
        }
    }
    return (pos == -1) ? nullptr : pRecs[pos]->pValue;
}

void TArrayTable::GoNext()
{
    if (!IsTabEnded())
        CurrPos++;
}

void TArrayTable::SetCurrentPos(int pos)
{
    if (pos >= 0 && pos < DataCount)
        CurrPos = pos;

}
