#ifndef __TABRECORD_H__
#define __TABRECORD_H__

#include <string>
#include "tdatvalue.h"

using namespace std;

class TTabRecord;
typedef string TKey;     // ��� ����� ������
typedef TTabRecord *PTTabRecord;

// ����� ��������-�������� ��� ������� �������
class TTabRecord : public TDatValue 
{  
protected:
    TKey Key;           // ���� ������
    PTDatValue pValue;  // ��������� �� ��������
public:
    TTabRecord(TKey k = "", PTDatValue pVal = nullptr) : Key(k), pValue(pVal) {}
    void SetKey(TKey k) { Key = k; }
    TKey GetKey() { return Key; }
    void SetValuePtr(PTDatValue p) { pValue = p; }
    PTDatValue GetValuePTR() { return pValue; }
    virtual TDatValue * GetCopy()
    {
        return new TTabRecord(Key, pValue);
    }
    TTabRecord & operator = (TTabRecord &tr)
    {
        if (!(*this == tr))
        {
            Key = tr.Key;
            pValue = tr.pValue;
        }
        return *this;
    }
    virtual int operator == (const TTabRecord &tr) { return Key == tr.Key; }
    virtual int operator < (const TTabRecord &tr) { return Key < tr.Key; }
    virtual int operator > (const TTabRecord &tr) { return Key > tr.Key; }
    //������������� ������ ��� ��������� ����� ������, ��. �����
    friend class TArrayTable;
    friend class TScanTable;
    friend class TSortTable;
    friend class TTreeNode;
    friend class TTreeTable;
    friend class TArrayHash;
    friend class TListHash;
};

#endif